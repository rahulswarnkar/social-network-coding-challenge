# Install

`npm i`

# Test

`npm test`

# Run

`npm start`

`npm run dev` (with swagger debug messages)

# Api

http://localhost:3000

## Docs

Swagger UI: http://localhost:3000/docs

### Post message
`POST /v1/messages`

See Swagger doc for schema.

### Wall (get messages)
`GET /v1/messages?username={username}`

### Follow another user
`POST /v1/users/{influencerUsername}/_follow`

User who wants to follow should be in request payload. See Swagger doc for schema.

### Timeline
`GET /v1/timeline?username={username}`