'use strict';

var app = require('express')();
var http = require('http');
var swaggerTools = require('swagger-tools');
var morgan = require('morgan')

var serverPort = 3000;

var options = {
  controllers: './src/handlers',
  useStubs: false // Conditionally turn on stubs (mock mode)
};

var swaggerDoc = require('./api/swagger.json');

app.use(morgan('dev'))

swaggerTools.initializeMiddleware(swaggerDoc, function (middleware) {
  app.use(middleware.swaggerMetadata());
  app.use(middleware.swaggerValidator());
  app.use(middleware.swaggerRouter(options));
  app.use(middleware.swaggerUi());

  http.createServer(app).listen(serverPort, function () {
    console.log('Your server is listening on port %d (http://localhost:%d)', serverPort, serverPort);
    console.log('Api docs available at http://localhost:%d/docs',  serverPort);
  });
});