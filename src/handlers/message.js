var users = require('../repositories/users')
var messages = require('../repositories/messages')

function addMessage(req, res, next) {
  if(req.body.message.length > 140) {
     res.status(400).end('Message too long')
     return
  }
  var user = users.getByName(req.body.username)
  if(!user) users.addUser(req.body.username)
  
  messages.addMessage(req.body)
  res.status(201).end()
}

function findMessagesByUser(req, res, next) {
  var m = messages.getByUsers([req.query.username], req.query.skip, req.query.limit)
  res.json(m)
}

function findMessagesByInfluencers(req, res, next) {
  var user = users.getByName(req.query.username)
  var usernames = user ? user.follows : []
  var m = messages.getByUsers(usernames, req.query.skip, req.query.limit)
  res.json(m)
}

module.exports = {
  addMessage: addMessage,
  findMessagesByUser: findMessagesByUser,
  findMessagesByInfluencers: findMessagesByInfluencers
}