var users = require('../repositories/users')

function getUserByName (req, res, next) {
  var username = req.swagger.params.username.value
  var user = users.getByName(username)
  if(!user) res.status(404).end('User not found')
  res.end(JSON.stringify(user, null, 2))
};

function followUser (req, res, next) {
  var influencerUsername = req.swagger.params.username.value
  var followerUsername = req.body.username

  if(influencerUsername === followerUsername) {
    res.status(400).end('Cannot follow itself')
    return
  }

  if(!users.getByName(influencerUsername)){
     res.status(404).end('User ' + influencerUsername + ' not found')
     return
  }
  
  if(!users.getByName(followerUsername)){
    res.status(404).end('User ' + followerUsername + ' not found')
    return
  }
  
  users.addInfluencer(followerUsername, influencerUsername)

  res.status(201).end()
};


module.exports = {
    getUserByName: getUserByName,
    followUser: followUser
}