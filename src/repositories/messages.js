var db = require('./db')
var _ = require('lodash')
var moment = require('moment')

function addMessage(message){
  var m = Object.assign({}, message, {timestamp: moment().toISOString()})
  db.messages.push(m)
}

function getByUsers(usernames, skip = 0, limit = 10){
  if(skip < 0) skip = 0
  if(limit < 0) limit = 0
  return _.chain(db.messages)
    .filter(m => {
      return _.find(usernames, u => u === m.username)
    })
    .sort(['timestamp'])
    .reverse()
    .slice(skip)
    .take(limit)
    .value()
}

module.exports = {
    addMessage: addMessage,
    getByUsers: getByUsers
}