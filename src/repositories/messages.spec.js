var proxyquire = require('proxyquire')

var db = {
  users: [],
  messages: []
}
proxyquire('./messages',{
  './db': db
})

var chai = require('chai')
var expect = chai.expect

var messages = require('./messages')

describe('messages', ()=>{
  it('is should have all methods', ()=>{
    expect(messages.addMessage).to.be.a('function')
    expect(messages.getByUsers).to.be.a('function')
  })

  describe('addMessage', ()=>{
    before(()=>{
      db.messages = []
    })
    it('should add message to database', ()=>{
        messages.addMessage([{
            username: 'alice',
            message: 'Hello world!'
        }])
        expect(db.messages).to.have.lengthOf(1)
    })
  })

  describe('getByUsers', ()=>{
    before(()=>{
      db.messages = [{
            timestamp: '2017-08-02T07:59:47.549Z',
            username: 'alice',
            message: 'Hello world!'
        },{
            timestamp: '2017-08-02T07:58:47.549Z',
            username: 'alice',
            message: 'Hello world!'
        },{
            timestamp: '2017-08-02T07:58:47.549Z',
            username: 'bob',
            message: 'Hello world!'
        }]
    })
    it('should add message to database', ()=>{
        expect(messages.getByUsers(['alice'])).to.have.lengthOf(2)
    })
  })

})