var db = require('./db')
var _ = require('lodash')

function getByName(username){
  return _.find(db.users, user => user.username === username)
}

function addUser(username){
  db.users.push({
    username:username,
    follows: []
  })
}

function addInfluencer(username, influencerUsername){
  var user = getByName(username)
  if(!_.find(user.follows, i => i === influencerUsername)){
    user.follows.push(influencerUsername)
  }
}

module.exports = {
  getByName: getByName,
  addUser: addUser,
  addInfluencer: addInfluencer
}