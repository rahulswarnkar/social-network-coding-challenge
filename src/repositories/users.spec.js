var proxyquire = require('proxyquire')

var db = {
  users: [],
  messages: []
}
proxyquire('./users',{
  './db': db
})

var chai = require('chai')
var expect = chai.expect

var users = require('./users')

describe('users', ()=>{
  it('is should have all methods', ()=>{
    expect(users.addUser).to.be.a('function')
    expect(users.getByName).to.be.a('function')
    expect(users.addInfluencer).to.be.a('function')
  })

  describe('addUser', ()=>{
    before(()=>{
      db.users = []
    })
    it('should add user to database', ()=>{
      users.addUser('username')
      expect(db.users).to.have.lengthOf(1)
      expect(db.users).to.deep.include({username: 'username', follows: []})
    })
  })

  describe('getByName', ()=>{
    before(()=>{
      db.users = [{
        username: 'username1',
        follows: []
      },{
        username: 'username2',
        follows: []
      }]
    })
    it('should get user by name', ()=>{
      expect(users.getByName('username1')).to.eql({username: 'username1', follows: []})
    })

  })

  describe('addInfluencer', ()=>{
    before(()=>{
      db.users = [{
        username: 'follower',
        follows: []
      },{
        username: 'influencer',
        follows: []
      }]
    })
    it('should add influencer to user', ()=>{
      users.addInfluencer('follower', 'influencer')
      expect(db.users).to.deep.include({username: 'follower', follows: ['influencer']})
    })

  })
})